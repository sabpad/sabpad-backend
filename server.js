// This is the server that serves the whole application
const http = require('http');
const app = require('./src');

const port = process.env.PORT || 5111;
const server = http.createServer(app);

server.listen(port, () => console.log(`App listening on port ${port}`));
