// Connects to MongoDB (for Mongoose and node-bifrost)
const mongoose = require('mongoose');
const Bifrost = require('node-bifrost');
const initRoles = require('./aclRolesInit');

connect().then(() => console.log('App is ready to go'));

function connect (dbUrl) {
    return new Promise((resolve, reject) => {
        dbUrl = dbUrl || process.env.DB_URL;

        global.bifrost = new Bifrost(dbUrl, {
            cb() {
                console.log('node-bifrost successfully connected');

                mongoose.connect(dbUrl, { useNewUrlParser: true }, err => {
                    if (err) return reject(err);

                    console.log('Mongoose successfully connected');
                    resolve(bifrost);
                });
            },
            err_cb(err) {
                reject(err)
            },
        });

        // Initializing the roles
        initRoles(bifrost);
    });
};
