const mongoose = require('mongoose');

module.exports = function (req, res) {
    // For a link such as https://api.sabpad.com/v1/email-verification?usid=abcxyz&token=blahblahblah
    if (!req.query.token || !req.query.usid) {
        return res.render('email-verification/failed');
    }

    var User = mongoose.model('user');
    User.findOne({ sid: req.query.usid }, function (err, user) {
        if (err || !user) {
            return res.render('email-verification/failed');
        }

        User.verifyToken(req.query.token, user.email, user.sid)
            .then(() => {
                user.emailVerified = true;
                user.save((err, user) => {
                    if (err) return res.render('email-verification/failed');

                    if (user.emailVerified) {
                        res.render('email-verification/verified');
                    } else {
                        res.render('email-verification/failed')
                    }
                });
            })
            .catch(err => res.render('email-verification/failed'));
    });
}
