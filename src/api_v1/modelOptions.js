/**
 * These are the options that can be set for a model.
 * The defaults are set below.
 */
module.exports = {
    public: true,
    // A list of routes that doesn't need authentication
    openRoutes: {
        get: ['/ping'],
        post: [],
        put: [],
        delete: [],
    },
    openInstanceRoutes: {
        get: ['/ping'],
        post: [],
        put: [],
        delete: [],
    },
    hooks: {
        before: {
            get: [],
            post: [],
            put: [],
            delete: [],
        },
        after: {
            get: [],
            post: [],
            put: [],
            delete: [],
        },
    },
    instanceHooks: {
        before: {
            get: [],
            post: [],
            put: [],
            delete: [],
        },
        after: {
            get: [],
            post: [],
            put: [],
            delete: [],
        },
    },
    noRemoteUpdateFields: [
        // List of fields/paths that shouldn't be possible to be updated from outside.
        'sid',
        'shortid',
        'id',
        '_id',
    ],
    methods: [
        // Routes that will be mounted onto /{modelName}/
        {
            route: '/ping', // accessible at e.g. /v1/books/ping
            verb: 'get',
            handler(req, res) {
                res.send('Pong');
            },
        }
    ],
    instanceMethods: [
        // Routes that will be mounted onto /{modelName}/{id}/
        {
            route: '/ping', // accessible at e.g. /v1/books/2ea1b0/ping
            verb: 'get',
            handler(req, res) {
                res.send(`${req.item.sid} says, "Pong"`);
            },
        }
    ],
};
