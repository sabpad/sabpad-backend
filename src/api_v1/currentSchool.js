const mongoose = require('mongoose');
const school = mongoose.model('school');

module.exports = currentSchool;

/**
 * Returns a middleware that extracts school id from the query parameter
 * `school` and defines `req.school`, which is a promise which resolves
 * to the current school.
 * @returns {Function} Express middleware
 */
function currentSchool() {
    return function (req, res, next) {
        req.school = new Promise((resolve, reject) => {
            var schoolId = req.query.school;
            if (schoolId) {
                school.findOne({ sid: schoolId })
                    .populate('chain')
                    .exec(function (err, school) {
                        if (err) return reject(err);
                        if (!school) return reject(`No school with id '${schoolId}' exists`);
                        resolve(school);
                    });
            } else {
                reject('Please provide a valid school id in query parameter `school`. e.g. ?school=<schoolId>');
            }
        });

        // Because UnhandledPromiseRejections are deprecated, handling this error with a dummy function
        req.school.catch(err => {});

        next();
    };
};
