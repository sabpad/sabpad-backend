/**
 * This is a MongooseJs plugin (https://mongoosejs.com/docs/plugins.html)
 * It adds a `shortid` property to every model. It's a unique ID, but is short.
 */
const shortid = require('shortid');

module.exports = function (schema, options) {
    // Add `shortid` property
    schema.add({
        sid: {
            type: String,
            default: shortid.generate,
            alias: 'shortid',
            unique: true,
        },
    });
};
