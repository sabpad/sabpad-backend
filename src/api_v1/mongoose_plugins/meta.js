/*
A mongoose plugin that adds empty meta static methods to models
*/
module.exports = function (schema, options) {
    if (typeof schema.statics.meta != 'function') {
        schema.statics.meta = function () {
            return [];
        }
    }
};
