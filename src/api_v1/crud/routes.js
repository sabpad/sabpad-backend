const express = require('express');
const safe = require('./safe');
const hook = require('./hook');
const instanceRoutes = require('./instanceRoutes');
const mountCustomRoutes = require('./mountCustomRoutes');
const dynamicAuthFilter = require('./dynamicAuthFilter');

module.exports = function (model) {
    var options = model.options;
    var name = model.name;
    var model = model.model;
    var hooks = options.hooks;
    var hooksCache = {};

    // The following are the static methods
    var router = express.Router();

    // Mount custom methods/routes
    mountCustomRoutes(options.methods, router, name, options.openRoutes);

    // Universal routes
    router.get(`/`,
        dynamicAuthFilter('/', options.openRoutes, 'get'),
        hook(hooks, 'before.get', hooksCache),
        async function (req, res) {
            // Some filtering
            var options = {};
            if (req.query.options) {
                try {
                    options = JSON.parse(req.query.options);
                } catch (err) {
                    return res.status(400).end('Invalid options! Please submit only valid JSON.');
                }
            }

            // Fetching data with or without filters
            let items = await model.find(filterGenerator(options.filters), null, paginationGenerator(options.pagination));
            res.send(items.map(i => safe(i)));

            hook(hooks, 'after.get', hooksCache)(req, res);
        }
    );

    router.get(`/count`,
        dynamicAuthFilter('/count', options.openRoutes, 'get'),
        hook(hooks, 'before.get/count', hooksCache),
        async function (req, res) {
            let count = await model.count();
            res.send({ count });

            hook(hooks, 'after.get/count', hooksCache)(req, res);
        }
    );

    router.get(`/meta`,
        dynamicAuthFilter('/meta', options.openRoutes, 'get'),
        hook(hooks, 'before.get/meta', hooksCache),
        function (req, res) {
            // If meta exists, then fetch it
            var meta = model.meta();
            res.json(meta);

            hook(hooks, 'after.get/meta', hooksCache)(req, res);
        }
    );

    router.post(`/`,
        dynamicAuthFilter('/', options.openRoutes, 'post'),
        hook(hooks, 'before.post', hooksCache),
        async function (req, res) {
            try{
                let item = new model(req.body);
                let success = await item.save();

                if (success) {
                    res.send(safe(item));
                } else {
                    res.sendStatus(500);
                }
            } catch (error) {
                res.sendStatus(500);
            }

            hook(hooks, 'after.post', hooksCache)(req, res);
        }
    );

    router.use(`/:sid`, instanceRoutes(model, options, name));

    return router;
}

//////////////////////////////////////////////////
function filterGenerator(filters) {
    var operatorsMap = {
        //'like': '$regex',
        '==': '$eq',
        '>': '$gt',
        '<': '$lt',
        '>=': '$gte',
        '<=': '$lte',
    };

    var query = {};
    filters.forEach(filter => {
        if (typeof filter.field == 'string' && filter.field.trim() == '') return;

        if (filter.operator.toLowerCase() == 'like') {
            query[filter.field] = { $regex: RegExp(filter.value) };
        }
        else if (typeof operatorsMap[filter.operator] != 'undefined') {
            query[filter.field] = {};
            query[filter.field][operatorsMap[filter.operator]] = filter.value;
        }
    });

    return query;
}
function paginationGenerator(pagination) {
    pagination = Object.assign({
        descending: false,
        page: 1,
        rowsPerPage: 20,
        sortBy: null,
        totalItems: 0
    }, pagination);

    var result = {};

    if (pagination.sortBy) {
        result.sort = {}
        result.sort[pagination.sortBy] = pagination.descending ? -1 : 1;
    }

    if (pagination.rowsPerPage >= 0) result.limit = pagination.rowsPerPage;
    result.skip = (pagination.page - 1) * pagination.rowsPerPage;

    return result;
}
