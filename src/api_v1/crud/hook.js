module.exports = hook;

/**
 * Acts as a cushion of comfort for the hook executors
 * @param {Object} allGivenHooks All the hooks mentioned in options
 * @param {String} type A string in the format of <phase>.<method>
 * @param {Object} cache A cache object that stores hooks once generated for later used
 * @returns {Function}
 */
function hook(allGivenHooks = {}, type = 'before.get', cache = null) {
    // Check if hook exists in cache
    if (cache) {
        if (typeof cache[type] != 'undefined') return cache[type];
    }

    // Clone a copy to prevent alter of the original copy of options.hooks
    var allHooks = Object.assign({}, allGivenHooks);

    // Phase
    let splitType = type.split('.');

    if (!splitType[0]) splitType[0] = 'before';
    var phase = splitType[0].toLowerCase();

    if (!splitType[1]) splitType[1] = 'get';
    var verb = splitType[1].toLowerCase();

    // Checking if defined
    if (typeof allHooks[phase] != 'object') {
        allHooks[phase] = {
            get: [],
            post: [],
            put: [],
            delete: [],
        };
    } else if (typeof allHooks[phase][verb] == 'undefined') {
        allHooks[phase][verb] = [];
    }

    var hooks = allHooks[phase][verb];
    if (phase == 'before') var hook = hookBefore(hooks);
    else var hook = hookAfter(hooks);

    // Cache the hook
    if (cache) {
        cache[type] = hook;
    }

    return hook;
}

/**
 * Before hooks executor.
 * @param {[Function]} hooks Array of functions with signature (req, res)
 * @returns {Function}
 */
function hookBefore(hooks) {
    return function (req, res, next) {
        var promises = getHookPromises(hooks, req, res);

        Promise.all(promises)
            .then(v => {
                var ok = true;
                for (var i = 0; i < v.length; i++) {
                    if (!(v[i] || typeof v[i] == 'undefined')) {
                        ok = false;
                        break;
                    }
                }

                if (ok) next();
                else res.sendStatus(403);
            })
            .catch(err => {
                res.sendStatus(500);
                console.error(err);
            });
    };
}

/**
 * After hooks executor.
 * @param {[Function]} hooks Array of functions with signature (req, res)
 * @returns {Function}
 */
function hookAfter(hooks) {
    return function (req, res) {
        var promises = getHookPromises(hooks, req, res);
    };
}

/**
 * Executes hooks and returns a array of promises
 * @param {[Function]} hooks Array of functions with signature (req, res)
 * @param {*} req Express request object
 * @param {*} res Express response object
 * @returns {Array}
 */
function getHookPromises(hooks, req, res) {
    if (!Array.isArray(hooks)) hooks = [hooks];
    var promises = [];

    // Running the hooks one-by-one
    hooks.forEach(hook => promises.push(typeof hook == 'function' ? hook(req, res) : hook));
    return promises;
}
