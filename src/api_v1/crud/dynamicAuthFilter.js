const passport = require('../passport');

/**
 * A auth filter that determines whether a route needs authentication or not based upon defined rules.
 * All routes are protected by default unless explicitly opened.
 * @param {String} route The route to check
 * @param {Object} openRules The rules to check againts
 * @param {String} verb The verb for which the route should be checked with. Default: get
 * @returns {Function} A middleware
 */
function dynamicAuthFilter(route, openRules = {}, verb = 'get') {
    const authFilter = passport.authenticate('jwt', { session: false });
    const noFilter = (req, res, next) => next();

    // We want auth only if it's enabled in env
    if (process.env.AUTH == 1) {
        // Check if there are any rules for the given verb
        if (openRules[verb] && Array.isArray(openRules[verb])) {
            // Now check if the given route is opened in the rules?
            if (openRules[verb].includes(route)) {
                return noFilter;
            }
        }

        // All routes are protected by default
        return authFilter;
    }
    
    return noFilter;
}

module.exports = dynamicAuthFilter;
