const express = require('express');
const hook = require('./hook');
const safe = require('./safe');
const mountCustomRoutes = require('./mountCustomRoutes');
const dynamicAuthFilter = require('./dynamicAuthFilter');

module.exports = instanceRoutesMounter;

function instanceRoutesMounter(model, options, modelName) {
    return function (req, res, next) {
        req.item = null;
        model.findOne({ sid: req.params.sid })
            .exec(function (err, item) {
                if (err) {
                    return res.sendStatus(500);
                }

                if (!item) {
                    return res.sendStatus(404);
                }

                req.item = item;
                var instanceRouter = instanceRoutes(options);

                // Mount custom instance methods/routes
                mountCustomRoutes(options.instanceMethods, instanceRouter, modelName, options.openInstanceRoutes);

                // Mounting universal instance routes
                instanceRouter(req, res, next);
            });
    }
}

function instanceRoutes(modelOptions) {
    var hooksCache = {};
    var hooks = modelOptions.instanceHooks;

    // The following are the instance methods
    var instanceRouter = express.Router();

    instanceRouter.get(`/`,
        dynamicAuthFilter('/', modelOptions.openInstanceRoutes, 'get'),
        hook(hooks, 'before.get', hooksCache),
        function (req, res) {
            res.send(safe(req.item));

            // After hook
            hook(hooks, 'after.get', hooksCache)(req, res);
        }
    );

    instanceRouter.put(`/`,
        dynamicAuthFilter('/', modelOptions.openInstanceRoutes, 'get'),
        hook(hooks, 'before.put', hooksCache),
        function (req, res) {
            for (var field in req.body) {
                if (!modelOptions.noRemoteUpdateFields.includes(field)) {
                    req.item[field] = req.body[field];
                }
            }

            req.item.save(function (err, item) {
                if (err) {
                    res.sendStatus(500);
                } else {
                    res.send(safe(item));
                }

                // After hook
                hook(hooks, 'after.put', hooksCache)(req, res);
            });
        }
    );

    instanceRouter.delete(`/`,
        dynamicAuthFilter('/', modelOptions.openInstanceRoutes, 'get'),
        hook(hooks, 'before.delete', hooksCache),
        function (req, res) {
            model.deleteOne({ sid: req.item.shortid }, function (err, success) {
                if (err) {
                    res.sendStatus(500);
                }
                else if (success) {
                    res.sendStatus(200);
                } else {
                    res.sendStatus(404);
                }


                // After hook
                hook(hooks, 'after.delete', hooksCache)(req, res);
            });
        }
    );

    return instanceRouter;
}
