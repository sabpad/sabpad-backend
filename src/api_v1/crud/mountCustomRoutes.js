const dynamicAuthFilter = require('./dynamicAuthFilter');

module.exports = mountCustomRoutes;

/**
 * Mount custom routes
 * @param {Array} methods Array of custom methods
 * @param {Object} router Express router instance
 * @param {String} modelName Name of the current model. Needed to print error messages is necessary.
 * @param {Object} authRules The rules to check auth againts
 */
function mountCustomRoutes(methods, router, modelName, authRules) {
    if (Array.isArray(methods)) {
        methods.forEach(method => {
            if (!method.route || typeof method.handler != 'function') {
                throw new Error(`In model ${modelName}, there is an invalid Method defined`);
            }

            // Get the verb, default is get
            method.verb = (method.verb || 'get').toLowerCase();
            var route = router.route(method.route);
            
            if (typeof route[method.verb] == 'function') {
                route[method.verb](dynamicAuthFilter(method.route, authRules, method.verb), method.handler);
            } else {
                throw new Error(`Unsupported HTTP verb '${method.verb}' defined for a method for the model '${modelName}'`);
            }
        });
    }
}
