const mongoose = require('mongoose');

module.exports = safe;
/**
 * Returns a safe-to-respond version of a document if available
 * @param {mongoose.Document} doc The document whose safe version is required
 * @param {Function} fnname The method name to call on the document. Default = safe
 * @returns doc
 */
function safe(doc = null, fnname = 'safe') {
    if (!doc instanceof mongoose.Document) return doc;

    if (typeof doc[fnname] == 'function') return doc[fnname]();
    else return doc.toObject();
}
