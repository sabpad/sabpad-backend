const router = require('./routes');
/**
 * A CRUD endpoint generator function.
 * @argument model Mongoose model exported from the app
 * @returns Express router
 */
module.exports = function crud(model) {
    return router(model);
}
