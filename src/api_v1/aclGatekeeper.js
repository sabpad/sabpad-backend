/**
 * A middleware that checks for ACL permission before allowing access
 * @param {Object} acl Instance of acl npm package
 */
async function aclGatekeeper(req, res, next) {
    /*
    Check permission.
    Take user from `req.session.userId`
    Take resource from `req.baseUrl`
    Take permission from `req.method`
    */
    try {
        var allowed = true;
        if (process.env.ACL == '1') {
            let resource = req.baseUrl.split('/')[2];
            let school = await req.school;

            allowed = await global.bifrost.allowed(req.user.id, resource, req.method.toLowerCase(), school);
        }

        if (allowed) next();
        else res.sendStatus(403);
    }
    catch (error) {
        res.sendStatus(500);
        console.error(error);
    }
};

module.exports = aclGatekeeper;
