const passport = require('./passport');
const mongoose = require('mongoose');
// Configure JWT strategy
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

var opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET || 'secret',
    issuer: process.env.JWT_ISSUER ||'sabpad.com',
    algorithms: ['HS256'],
    //audience: 'yoursite.net',
};

passport.use(new JwtStrategy(opts, function (payload, done) {
    var UserModel = mongoose.model('user');
    // Instead of loading every time from db, find a more efficient way
    UserModel.findOne({ sid: payload.sub }, function (err, user) {
        if (err) {
            console.error(err);
            return done(new Error('Internal server error. Please try later.'));
        }
        
        // User might not exist
        if (!user) {
            return done(null, false);
        }
        
        // All OK
        done(null, user);
    });
}));
