// Route handlers to handle logins and registers
const mongoose = require('mongoose');

// Getiing the model
var UserModel = mongoose.model('user');

module.exports.signin = async function (req, res) {
    // Email & Password are mandatory
    if (!req.body.email || !req.body.password) return res.sendStatus(401);

    UserModel.findOne({ email: req.body.email }, async function (err, user) {
        if (err) return res.sendStatus(401);

        // Check if user exist
        if (!user) return res.sendStatus(401);

        // Now check if the password is valid or not
        if (! await user.isPasswordValid(req.body.password)) return res.sendStatus(401);

        // All OK! Issue token
        var token = await user.generateToken();
        res.send({ token });
    });
};

// Register route handler
module.exports.signup = function (req, res) {
    var name = req.body.name;
    var email = req.body.email;
    var password = req.body.password;

    var user = new UserModel({ name, email });

    user.setPassword(password).then(() => {
        // Save the user
        user.save(async (err, user) => {
            if (err) {
                return res.status(500).end(err.message);
            }
            var response = user.safe();

            // If token is required, then generate it
            if (req.query.gentoken == 'yes') {
                response.token = await user.generateToken();
            }

            res.json(response);

            // Send verification email
            user.getVerificationToken(user.email).then(token => {
                // Send email #WIP
            });
        });
    }).catch(err => res.status(500).end(err.message));
};
