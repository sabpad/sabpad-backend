require('./init');
const express = require('express');
const passport = require('./passport');
const models = require('../models');
const crud = require('./crud');
const defopts = require('./modelOptions');
const aclGatekeeper = require('./aclGatekeeper');
const { signin, signup } = require('./signin_signup');
const currentSchool = require('./currentSchool');
const emailVerifier = require('./emailVerifier');

// Configures JWT strategy
require('./auth');

module.exports = function () {
    const router = express.Router();

    // Initializing passport
    router.use(passport.initialize());

    // Login & register routes, these routes aren't auth protected
    router.post('/signin', signin);
    router.post('/signup', signup);
    router.get('/email-verification', emailVerifier);

    // Fetch school id
    router.use(currentSchool());

    // Endpoit to fetch acl
    router.get('/acl', function (req, res) {
        res.json(global.bifrost.aclist);
    });

    for (var model in models) {
        if (!models.hasOwnProperty(model)) continue;
        model = models[model];

        // Setting up model options
        model.options = prepareModelOptions(model.options, defopts);

        // Check if model is public
        if (!model.options.public) continue;

        // Mounting the CRUD routes
        let path = '/' + model.name;
        router.use(path, aclGatekeeper, crud(model));
    }

    return router;
};

/**
 * Merges defined and default options. It does deep-merge.
 * @param {Object} definedOptions Custom defined options
 * @param {Object} defaultOptions Options defined by default
 */
function prepareModelOptions(definedOptions, defaultOptions) {
    var appendLists = [
        'openRoutes.get',
        'openRoutes.post',
        'openRoutes.put',
        'openRoutes.delete',
        'openInstanceRoutes.get',
        'openInstanceRoutes.post',
        'openInstanceRoutes.put',
        'openInstanceRoutes.delete',
        'hooks.before.get',
        'hooks.before.post',
        'hooks.before.put',
        'hooks.before.delete',
        'hooks.after.get',
        'hooks.after.post',
        'hooks.after.put',
        'hooks.after.delete',
        'instanceHooks.before.get',
        'instanceHooks.before.post',
        'instanceHooks.before.put',
        'instanceHooks.before.delete',
        'instanceHooks.after.get',
        'instanceHooks.after.post',
        'instanceHooks.after.put',
        'instanceHooks.after.delete',
        'noRemoteUpdateFields',
        'methods',
        'instanceMethods',
    ];

    var definedOpts = Object.assign({}, definedOptions);
    var defaultOpts = Object.assign({}, defaultOptions);
    
    appendLists.forEach(list => {
        var listsgmt = list.split('.');

        // Browse the object
        var subObjDefined = definedOpts;
        var subObjDefault = defaultOpts;

        for (var i = 0; i < listsgmt.length - 1; i++) {
            if (
                typeof subObjDefined[listsgmt[i]] == 'undefined' ||
                typeof subObjDefault[listsgmt[i]] == 'undefined'
            ) {
                return;
            } else {
                subObjDefined = subObjDefined[listsgmt[i]];
                subObjDefault = subObjDefault[listsgmt[i]];
            }
        }

        // i should be he index of the last listsgmt
        i = listsgmt.length - 1;

        if (Array.isArray(subObjDefined[listsgmt[i]]) && Array.isArray(subObjDefault[listsgmt[i]])) {
            subObjDefined[listsgmt[i]] = subObjDefault[listsgmt[i]].concat(subObjDefined[listsgmt[i]]);
        }
    });

    return Object.assign(defaultOpts, definedOpts);
}
