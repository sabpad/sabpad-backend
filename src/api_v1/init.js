// This script runs some code for initialization

// Add a Mongoose global plugin `shortid`
const mongoose = require('mongoose');
const shortidPlugin = require('./mongoose_plugins/shortid');
const metaPlugin = require('./mongoose_plugins/meta');

mongoose.plugin(shortidPlugin);
mongoose.plugin(metaPlugin);
