// This script runs some code for initialization

// Spit out some settings in console
if (process.env.ACL == 0) {
    console.log('#> ACL is turned OFF');
} else {
    console.log('#> ACL is turned ON')
}

if (process.env.AUTH == 0) {
    console.log('#> Authentication is turned OFF');
} else {
    console.log('#> Authentication is turned ON')
}
