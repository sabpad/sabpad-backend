const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    marks: {
        type: Number,
        default: 0,
    },
    student: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    },
    paper: {
        type: Schema.Types.ObjectId,
        ref: 'exam-paper',
    },
    
    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Marks',
            value: 'marks',
            type: 'number',
            options: {
                min: 0,
            }
        },
        {
            text: 'Student',
            value: 'student',
            type: 'object',
            model: 'user',
            meta: mongoose.model('user').meta(),
        },
        {
            text: 'Examination paper',
            value: 'paper',
            type: 'object',
            model: 'exam-paper',
            meta: mongoose.model('exam-paper').meta(),
        }
    ]
}

module.exports.name = 'exam-marks';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
