const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const gravatar = require('gravatar');
const jwt = require('jsonwebtoken');

const Schema = mongoose.Schema;
const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const theSchema = new Schema({
    name: String,
    pwdhash: {// bcrypt hash
        type: String,
    },
    email: {
        type: String,
        match: [emailRegex, 'Must be a valid email'],
        required: true,
        unique: true,
    },
    emailVerified: false,
    avatar: {
        type: String,
        default: function () {
            return gravatar.url(this.email, {
                size: 150,
                default: 'mp',
            }, true);
        },
    },
});

/**
 * We can use this function to verify anything (e.g. email, phone) related to the user
 */
theSchema.statics.verifyToken = function (token, thing, user) {
    return new Promise((resolve, reject) => {
        var opts = {
            algorithm: 'HS256',
            issuer: process.env.JWT_ISSUER || 'sabpad.com',
            subject: user,
        };
        var secret = process.env.JWT_SECRET || 'secret';
        jwt.verify(token, secret, opts, (err, payload) => {
            if (err) return reject(err);

            if (payload.thing == thing) {
                resolve();
            } else {
                reject(`Invalid token`);
            }
        });
    });
}
/**
 * We will get the verification token from here
 */
theSchema.methods.getVerificationToken = function (thing = '', expiresIn = '1d') {
    return new Promise((resolve, reject) => {
        var opts = {
            algorithm: 'HS256',
            expiresIn,
            issuer: process.env.JWT_ISSUER || 'sabpad.com',
            subject: this.sid,
        };
        var secret = process.env.JWT_SECRET || 'secret';
        var payload = {
            thing
        };

        jwt.sign(payload, secret, opts, (err, token) => {
            if (err) reject(err);
            else resolve(token);
        });
    });
}

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text'
        },
        {
            text: 'Email',
            value: 'email',
            type: 'email',
        },
        {
            text: 'Avatar',
            value: 'avatar',
            type: 'file',
        },
        {
            text: 'Email verified?',
            valid: 'emailVerified',
            type: 'boolean',
        }
    ]
}

// Password
theSchema.methods.setPassword = function (p) {
    return new Promise(async (resolve, reject) => {
        try {
            this.pwdhash = await bcrypt.hash(p, 10);
            resolve(true);
        }
        catch (error) {
            reject(error);
        }
    });
};
theSchema.methods.isPasswordValid = function (p) {
    return new Promise(async (resolve, reject) => {
        try {
            let valid = await bcrypt.compare(p, this.pwdhash);
            resolve(valid);
        }
        catch (error) {
            reject(error);
        }
    });
};

theSchema.methods.safe = function () {
    return {
        sid: this.sid,
        name: this.name,
        avatar: this.avatar,
        email: this.email,
    }
}

theSchema.methods.generateToken = function () {
    return new Promise((resolve, reject) => {
        var opts = {
            algorithm: 'HS256',
            expiresIn: '30 days',
            issuer: process.env.JWT_ISSUER || 'sabpad.com',
            subject: this.sid,
        };
        var secret = process.env.JWT_SECRET || 'secret';
        var payload = {};
    
        jwt.sign(payload, secret, opts, (err, token) => {
            if (err) {
                return reject(err);
            }
            resolve(token);
        });
    });
}

// Exporting the model
module.exports.name = 'user';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {
    hooks: {
        before: {
            get: false,
            'get/count': false,
            post: false,
        }
    },
    instanceHooks: {
        before: {
            get: false,
            put: false,
            delete: false,
        }
    },
    methods: [
        {
            route: '/me',
            verb: 'get',
            handler(req, res) {
                if (req.user) {
                    res.json(req.user.safe());
                } else {
                    res.sendStatus(404);
                }
            },
        }
    ]
};
