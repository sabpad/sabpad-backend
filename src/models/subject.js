const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    name: String,
    code: String,
    syllabus: String,
    type: {
        type: String,
        default: 'optional',
    },
    note: String,

    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text'
        },
        {
            text: 'Code',
            value: 'code',
            type: 'text'
        },
        {
            text: 'Syllabus',
            value: 'syllabus',
            type: 'text',
        },
        {
            text: 'Type',
            value: 'type',
            type: 'select',
            options: {
                items: [
                    { text: 'Compulsory', value: 'compulsory' },
                    { text: 'Optional', value: 'optional' },
                ]
            }
        },
        {
            text: 'Note(s)',
            value: 'note',
            type: 'text'
        }
    ]
}

module.exports.name = 'subject';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
