const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    name: String,
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text'
        }
    ]
}

module.exports.name = 'chain';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
