const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    name: String,
    category: String,
    note: String,

    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text'
        },
        {
            text: 'Category',
            value: 'category',
            type: 'text',
        },
        {
            text: 'Note(s)',
            value: 'note',
            type: 'text',
            sortable: false
        }
    ]
}

module.exports.name = 'fee-type';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
