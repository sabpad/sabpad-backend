const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    name: String,
    start: Date,
    end: Date,

    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text'
        },
        {
            text: 'Starting date/time',
            value: 'start',
            type: 'date'
        },
        {
            text: 'Ending date/time',
            value: 'end',
            type: 'date',
        }
    ]
}

module.exports.name = 'invoice-period';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
