/**
 * This script's job is to require all models, 
 * and export as one object.
 */
const fs = require('fs');

fs.readdirSync(__dirname).forEach(file => {
    // Don't import myself
    if (`${__dirname}/${file}` == __filename) return;

    let model = require('./' + file);
    module.exports[model.name] = model;
});

