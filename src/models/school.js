const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    name: String,
    chain: {
        type: Schema.Types.ObjectId,
        ref: 'chain',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text'
        },
        {
            text: 'Parent chain',
            value: 'chain',
            type: 'object',
            meta: mongoose.model('chain').meta(),
        }
    ]
}

module.exports.name = 'school';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
