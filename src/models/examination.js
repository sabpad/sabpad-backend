const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    name: String,
    session: {
        type: Schema.Types.ObjectId,
        ref: 'session',
    },
    papers: [{
        type: Schema.Types.ObjectId,
        ref: 'exam-paper',
    }],

    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text'
        },
        {
            text: 'Academic session',
            value: 'session',
            type: 'object',
            meta: mongoose.model('session').meta(),
        },
        {
            text: 'Exam papers',
            value: 'papers',
            type: 'array',
            meta: mongoose.model('exam-paper').meta(),
        },
    ];
}

module.exports.name = 'examination';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
