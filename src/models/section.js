const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    class: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'class',
    },

    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text'
        },
        {
            text: 'Class',
            value: 'class',
            type: 'object',
            meta: mongoose.model('class').meta(),
        }
    ]
}

module.exports.name = 'section';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
