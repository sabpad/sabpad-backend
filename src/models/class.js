const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    name: String,
    numeric_name: Number,

    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text'
        },
        {
            text: 'Numeric name',
            value: 'numeric_name',
            type: 'number',
        }
    ]
}

module.exports.name = 'class';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
