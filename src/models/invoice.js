const mongoose = require('mongoose');
const cfpg = require('../pg/cashfree');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    paid: {
        type: Boolean,
        default: false,
    },
    note: String,
    discount: {
        type: Schema.Types.ObjectId,
        ref: 'discount',
    },
    feeType: {
        type: Schema.Types.ObjectId,
        ref: 'fee-type',
    },
    invoicePeriod: {
        type: Schema.Types.ObjectId,
        ref: 'invoice-period',
    },
    session: {
        type: Schema.Types.ObjectId,
        ref: 'session',
    },
    student: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    },

    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Is paid?',
            value: 'paid',
            type: 'boolean',
        },
        {
            text: 'Note(s)',
            value: 'note',
            type: 'text',
            sortable: false,
        },
        {
            text: 'Discount',
            value: 'discount',
            type: 'object',
            meta: mongoose.model('discount').meta(),
        },
        {
            text: 'Fee type',
            value: 'feeType',
            type: 'object',
            model: 'fee-type',
            meta: mongoose.model('fee-type').meta(),
        },
        {
            text: 'Invoice period',
            value: 'invoicePeriod',
            type: 'object',
            model: 'invoice-period',
            meta: mongoose.model('invoice-period').meta(),
        },
        {
            text: 'Seesion',
            value: 'session',
            type: 'object',
            meta: mongoose.model('session').meta(),
        },
        {
            text: 'Student',
            value: 'student',
            type: 'object',
            model: 'user',
            meta: mongoose.model('user').meta(),
        },
    ];
}

module.exports.name = 'invoice';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {
    openRoutes: {
        post: ['/cfpg-return', '/cfpg-notify'],
    },
    openInstanceRoutes: {
        get: ['/cfpg-pay', '/invoice.html'],
    },
    methods: [
        {
            route: '/cfpg-return',
            method: 'post',
            handler: cfpg.return(),
        },
        {
            route: '/cfpg-notify',
            method: 'post',
            handler: cfpg.notify(),
        },
    ],
    instanceMethods: [
        {
            route: '/cfpg-pay',
            method: 'get',
            handler: cfpg.redir(),
        },
        {
            route: '/invoice.html',
            method: 'get',
            handler(req, res) {
                res.send(`<h1>Invoice #${req.item.sid}</h1>`);
            },
        },
    ]
};
