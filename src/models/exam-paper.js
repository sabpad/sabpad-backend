const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    date: Date,
    subject: {
        type: Schema.Types.ObjectId,
        ref: 'subject',
    },

    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Date',
            value: 'date',
            type: 'date'
        },
        {
            text: 'Subject',
            value: 'subject',
            type: 'object',
            meta: mongoose.model('subject').meta(),
        }
    ]
}

module.exports.name = 'exam-paper';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
