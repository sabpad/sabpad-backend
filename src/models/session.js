const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    start: Date,
    end: Date,

    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Starting on',
            value: 'start',
            type: 'date'
        },
        {
            text: 'Ending on',
            value: 'end',
            type: 'date',
        }
    ]
}

module.exports.name = 'session';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
