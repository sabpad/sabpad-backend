const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    name: String,
    type: {
        type: String,
        default: "percentage",
    },
    amount: {
        type: Number,
        default: 0,
    },
    note: String,
    
    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text',
        },
        {
            text: 'Type',
            value: 'type',
            type: 'select',
            options: {
                items: [
                    { text: 'Percentage', value: 'percentage'},
                    { text: 'Absolute', value: 'absolute'},
                ],
            }
        },
        {
            text: 'Amount',
            value: 'amount',
            type: 'number',
            options: {
                min: 0,
            }
        },
        {
            text: 'Note(s)',
            value: 'note',
            type: 'text',
            sortable: false,
        }
    ];
}

module.exports.name = 'discount';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
