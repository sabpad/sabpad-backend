const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const theSchema = new Schema({
    name: String,
    salary: {
        type: Number,
        default: 0,
    },

    // Relation with schools and chains
    ownerType: {
        type: String,
        enum: ['school', 'chain'],
        default: 'school',
    },
    owner: {
        type: Schema.Types.ObjectId,
        refPath: 'ownerType',
    },
});

theSchema.statics.meta = function () {
    return [
        {
            text: 'Name',
            value: 'name',
            type: 'text'
        },
        {
            text: 'Salary amount',
            value: 'salary',
            type: 'number',
            options: {
                min: 0,
            }
        }
    ]
}

module.exports.name = 'salary-grade';
module.exports.model = mongoose.model(module.exports.name, theSchema);
module.exports.options = {};
