require('dotenv').config();
require('./init');
require('./connect');
const express = require('express');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');
const api_v1 = require('./api_v1');

const app = express();
// Exporting the app
module.exports = app;

app.set('views', './src/views');
app.set('view engine', 'pug');

app.use(cors());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Mount v1 API
app.use('/v1', api_v1());

// 404
app.all('*', function (req, res) {
    res.sendStatus(404);
});
