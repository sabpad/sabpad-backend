/*
Integration for Cashfree payment gateway
*/
const crypto = require('crypto');

module.exports.redir = pg_redir;
module.exports.return = pg_return;
module.exports.notify = pg_notify;

// Payment route handlers
function pg_redir() {
    return function (req, res) {
        res.render('cashfree_pg_redir', {

        });
    }
}
function pg_return() {
    return function (req, res) {
        res.render('cashfree_pg_return', {
            success: req.query.s == 1 ? true : false,
            nextUrl: 'https://app.sabpad.com',
        });
    }
}
function pg_notify() {
    return function (req, res) {
        res.sendStatus(501);
    }
}

/**
 * Checks if signature given by cashfree is valid or not
 * @param {Object} data The data to be signed
 * @param {String} secret Account secret
 * @returns {Boolean}
 */
function verifySignature(data, secret) {
    // Append all data except `signature`
    signData = '';
    for (var key in data) {
        if (key != 'signature')
            signData += data[key];
    }

    // Create signature
    var sign = crypto.createHmac('sha256', secret)
        .update(signData)
        .digest('base64');

    // Verify
    return sign === data.signature;
}


/**
 * Generate signature for authenticating requests
 * @param {Object} data The data to be signed
 * @param {String} secret Account secret
 * @returns Signature
 */
function generateSignature(data, secret) {
    // Sort by the keys
    var orderedData = {};
    Object.keys(data).sort().forEach(function (key) {
        orderedData[key] = data[key];
    });

    // Convert into single string
    var signData = '';
    for (var key in orderedData) {
        signData += key + orderedData[key];
    }

    // Create signature
    var sign = crypto.createHmac('sha256', secret)
        .update(signData)
        .digest('base64');
    return sign;
}
