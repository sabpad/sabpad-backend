/*
Define all user roles and give appropriate
permissions to the roles.

It just defines top level permissions, like
which role has permission to view which resources.
It doesn't implement multi-tenancy. i.e. It doesn't 
define restrictions like "Principal of School A shouldn't
be able to view data of School B". That level of
rules are to be defined somewhere else.
*/
const roles = require('./roles');

module.exports = function (bifrost) {
    bifrost.allow(roles.superadmin, '*', '*');
    bifrost.allow(roles.chainHead, '*', '*');
    bifrost.allow(roles.chainAccountant, [
        'discount',
        'fee-type',
        'invoice-period',
        'invoice',
        'salary-grade',
    ], '*');
    bifrost.allow(roles.principal, '*', '*');
    bifrost.allow(roles.accountant, [
        'discount',
        'fee-type',
        'invoice-period',
        'invoice',
        'salary-grade',
    ], '*');
    bifrost.allow(roles.teacher, 'exam-marks', ['post', 'put']);
    bifrost.allow(roles.teacher, [
        'class',
        'section',
        'exam-marks',
        'exam-paper',
        'examination',
        'invoice',
        'routine-period',
        'subject',
    ], 'get');
    bifrost.allow(roles.parent, [
        'class',
        'section',
        'exam-marks',
        'exam-paper',
        'examination',
        'invoice',
        'routine-period',
        'subject',
    ], 'get');
    bifrost.allow(roles.student, [
        'class',
        'section',
        'exam-marks',
        'exam-paper',
        'examination',
        'invoice',
        'routine-period',
        'subject',
    ], 'get');
    bifrost.allow(roles.examHead, [
        'exam-marks',
        'exam-paper',
        'examination',
    ], '*');
    bifrost.allow(roles.examHead, 'subject', 'get');
}
