/*
Returns a new instance of passport.
This is done so that the different API
versions can use different instances of
passport seamlessly.
*/
const passport = require('passport');

module.exports = function () {
    return new passport.Passport();
};
