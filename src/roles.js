module.exports = {
    superadmin: "superadmin",
    chainHead: "chain-head",
    chainAccountant: "chain-accountant",
    principal: "principal",
    accountant: "accountant",
    teacher: "teacher",
    parent: "parent",
    student: "student",
    librarian: "librarian",
    warden: "warden",
    examHead: "exam-head",
    referrer: "referrer",
};
